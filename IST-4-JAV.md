---
title: IST-4-JAV: Java programming
---

# 

# Classes

- 2023-10-02 (mon./月): the [slides](IST-4-JAV/Class1.pdf), the [worksheet](IST-4-JAV/Worksheet1.pdf)
- 2023-10-04 (wed./水): the [slides](IST-4-JAV/Class2.pdf), the [worksheet](IST-4-JAV/Worksheet2.pdf)
- 2023-10-06 (fri./金): the [slides](IST-4-JAV/Class3.pdf), the [worksheet](IST-4-JAV/Worksheet3.pdf)
- 2023-10-11 (wed./水): the [slides](IST-4-JAV/Class4.pdf), the [worksheet](IST-4-JAV/Worksheet4.pdf)
- 2023-10-13 (fri./金): the [slides](IST-4-JAV/Class5.pdf), no worksheet! (let's
  just finish everything left over from the previous ones, get started on the
  assignment and ask questions while it's time)

# Evaluation

Due 2023-10-29 (sun./日): the [assignment](IST-4-JAV/Assignment.pdf)
