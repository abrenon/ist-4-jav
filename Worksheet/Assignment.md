---
title: Game project
---

The assignment to validate this course is to write a mini-game in Java. The game
may use a graphical user interface with one of the frameworks seen in class but
it doesn't have to. A pure text-mode game won't necessarily receive a
lower-grade than a GUI one or be graded more severely. It should consists of a
repository of Java code (some light scripting in other languages to package and
build it is ok) and must be sent to me before the 30th of october (strictly
before, that is, you have until 2023-10-29 23:59 UTC+1) under the form of a link
to a `git` repository or an archive (`.zip`, `.tar.gz`, `.tar.bz2` or `.7z`)
containing the sources and the sources only (**no compiled binary**).

The project is meant for you to demonstrate your command of the concepts studied
in class such as inheritance, typing or packages. The source code of your game
_must_ include interfaces and abstract classes, so I suggest to you some sort of
farming game where they occur naturally (because, usually, in those games the
objects found or used form a tree of families sharing common properties —
classes — or fulfilling some common purpose — interfaces) but there is no
obligation. If you choose a different kind of game, please let me know in
advance and explain how you plan to use interfaces and abstract classes in your
implementation.

There is no firm notation grid but the following criteria will be taken into
account to give you a grade between 0 and 20:

- Reuse of the object-oriented programming concepts seen in class:
  + are those concepts used correctly ?
  + are they used in a relevant way ?
- Technical quality of the project
  + have you documented properly the way to compile and use your program, for
    instance with a README file ? (you shouldn't assume I use the same
    development environment as you, i.e. "I just click the 'Run' button" is not
    a proper compilation instruction so in the end everything should be
    compilable fairly easily from the command line with `javac`)
  + is it compiling when following these instructions ?
  + does it run without flaws ? are there unhandled errors at run-time ? bugs ?
- Quality of the code
  + how easy to understand is your code ?
  + how well are your variables, functions and classes named ?
  + is your code clean from a typographic perspective ? (no extra spaces
    trailing on lines, consistent indentation — tabs or spaces, pick your side —
    and naming conventions — case, abbreviations or not). The code should be
    homogenous enough that one can't know who has written what parts just by
    looking at them
  + are the comments appropriate ? (have you used them to document how your code
    is used and works or abused them to caption the obvious ?)

You will need an overall grade of at least 10 and a strictly positive grade in
each of the first-level criteria above (concepts, technical quality, code
quality) to pass.

You may work on your project in groups of up to 3 students but I want all of you
to work on all the aspects of your program: it is not ok for one student to only
think about the gameplay while the other does the implementation. A global grade
will be given for the group. Once your group is formed or you have decided to
work alone please send me an e-mail to register your project.
