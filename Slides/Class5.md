---
title: IST-4-JAV Java Programming
subtitle: Class 5 - New heights
author: Alice BRENON `<alice.brenon@liris.cnrs.fr>`
date: 2022-10-19
institute:
	\includegraphics[height=.9cm]{figures/LIRIS}\quad
	\includegraphics[height=.9cm]{figures/INSA}
aspectratio: 169
header-includes:
	- \usepackage{fdsymbol}
	- \usepackage{textgreek}
	- \usepackage{bclogo}
	- \usepackage{Beamer/beamerthemePerso}
	- \setcounter{tocdepth}{1}
---

# Types

## Enumerations

### Need

```Java
class PureBreedCat extends DomesticCat {
	String breed;
	…
}
```

#### `String` ?

1. for user: what are the possible values ?
2. possible errors (case, typos…)
3. need to validate input
4. ($\Rightarrow$ what to do with unexpected values ?)

$\Rightarrow$ type too "large"

### A possible solution

:::columns
::::{.column width=40%}

need a finite number of values that are

- unique
- easy to compare
- associated to names

$\Rightarrow$ constants of any integral type will do !

::::
::::{.column width=60%}

. . .

```Java
final static byte SIAMESE = 0;
final static byte CHARTREUX = 1;
final static byte SAVANNAH = 2;
…
```

::::
:::

\vspace{0.5cm}

- 1 and 2 are fixed !
- 3 and 4 remain : (

### The `enum` types

- fixed set of **symbolic** constants
- introduced by `enum` instead of `class`
- names separated by `,` (ended by `;`)
- it's a class like any other ! (fields, methods)

```Java
enum CatBreed {
	SIAMESE, CHARTREUX, SAVANNAH
}

CatBreed breed = CatBreed.SIAMESE;
breed == CatBreed.SAVANNAH; // false
```

### Advanced example

- all values are constructed statically
- properties can be initialized
- optional constructor can't be `public`

```Java
enum BreedType {
	NATURAL, MUTATION, HYBRID
}
enum CatBreed {
	SIAMESE (BreedType.MUTATION),
	CHARTREUX (BreedType.NATURAL),
	SAVANNAH (BreedType.HYBRID);
	private BreedType type;
	CatBreed (BreedType type) { this.type = type; }
	BreedType getBreedType() { return this.type; }
}
```

## Type variables

### Remember containers ?

```Java
List<Integer>
List<Double>
List<String>
List<List<String>>
…
```

- a "family" of classes (infinite)
- work on any type

### Methods signatures

```Java
List<Integer> ints = new ArrayList<>();
List<String> strings = new ArrayList<>();
for(int i = 0; i < 10; i++) {
	ints.add(i);
	strings.add(Integer.toString(i));
}
```

$\rightarrow$ what is the signature of `.add` ?

### An "input"

```Java
void add(Integer n) // ??
void add(String s) // ??
```

- type of their methods depend on the type context
- $\rightarrow$ need to be named in the class declaration
- "input" of a class, like arguments are the inputs of a function
- **type variables**

### Generics

- an identifier (any variable name)
- inside the "diamond" `<>`
- usually a single letter:
	+ `N` $\rightarrow$ "number" type
	+ `K` $\rightarrow$ "key" in an association
	+ `V` $\rightarrow$ "value" in an association
	+ `T` $\rightarrow$ any type
- multiple parameters are comma separated `<K, V>`

### May appear

**in classes**

```Java
class Box<T> {
	…
}
```

**in methods**

as the last modifier, just before the return type

```Java
public <T> List<T> preview(List<T> full, int size);
```

`<T>` is not the return type, it's like a flag.

### Co/contravariance again

is `List<Integer>` a "subtype" of `List<Object>` ?

. . .

= "if I get a `List<Integer>`, do I have a `List<Object>` ?"

```Java
List<Integer> ints = new ArrayList<>();
ints.add(4);
```

. . .

:::columns
::::{.column width=35%}
…yes ?

- `Object`
- $\rightarrow$ `Number`
- $\hspace{1em}\rightarrow$ `Integer`

```Java
Object o = ints.get(0);
```
::::
::::{.column width=60%}
. . .

but
```Java
ints.set(0, o);
```

```
|  Error:
|  incompatible types:
|  java.lang.Object cannot be converted
|  to java.lang.Integer
|  ints.set(0, o)
```
::::
:::

### Wildcards

…but it's a subtype of `List<?>` !

- syntax: `?`
- the "unknown type"
- $\Rightarrow$ allows to get sub/supertypes !

#### Remark

no name $\Rightarrow$ strictly less powerful (!)

### Refine a bit

```Java
Object first(List<Object> l) {
  return l.size() > 0 ? l.get(0) : null;
}
```

\vspace{.5cm}

$\rightarrow$ cannot use on `ints`

. . .

\vspace{.5cm}

```Java
Object first(List<? extends Object> l) {
  return l.size() > 0 ? l.get(0) : null;
}
```

### Conversely

```Java
List<Object> stuff = new ArrayList<>();
void addInt(List<Integer> l) {
  l.add(4);
}
stuff.add(4); // works
addInt(stuff); // does not !
```

\vspace{.3cm}

$\rightarrow$ cannot use on `stuff`

. . .

\vspace{.3cm}

```Java
void addInt(List<? super Integer> l) {
  l.add(4);
}
addInt(ints); // still works
addInt(stuff); // works too !
```

### Bounds

:::columns
::::column

**`extends`**

- any type that derives from or implements the given type
- useful on covariant operations
- works for generic types and wildcards

```Java
<? extends Integer>
<T extends Integer>
```

::::
::::column

**`super`**

- any type which the given type derives from
- useful on contravariant operations
- only for wildcards ! not generics

```Java
<? super Integer>
```
::::
:::

can be combined with `&`: `<T extends Comparable & Number>`

### Use case

```Java
static <T,U extends Comparable<? super U>> Comparator<T>
    comparing(Function<? super T,? extends U> keyExtractor)
```

- `<T, U …>` : two type parameters (`T` and `U`)
- `U` must be comparable with any type inheriting it
- `Comparator<T>`: this returns a comparator (on objects of type `T`)
- `keyExtractor` a function
    + `<? super T>` from something `T` inherits from
    + `<? extends U>` to something that inherits `U`

"If you can map `T` to a type `U` which you know how to compare, you can compare
`T`"

## Error types

### Warning fellow developers

```Java
int divideByZero(int i) {
	return i / 0;
}
```

```
jshell> divideByZero(4)
|  Exception java.lang.ArithmeticException: / by zero
|        at divideByZero (#14:2)
|        at (#15:1)
```

*runtime* errors vs. *static* errors\
$\rightarrow$ could we make it a bit safer ?

### Syntax `throws`

```Java
int divideByZero(int i) throws ArithmeticException {
	return i / 0;
}
```

:::columns
::::{.column width=48%}
- `throw` in the code (statement)
- `throws` in the type
::::
::::{.column width=3%}
\vspace{.5cm}
$\uparrow$
::::
::::{.column width=40%}
. . .

\hrule

```Java
try {
```
…
```Java
} catch(SomeException e) {
```
…
```Java
} catch(OtherException e) {
```
…
```Java
}
```
::::
:::

### A real-life example

```Java
class FileInputStream extends InputStream {
```
…
```Java
  FileInputStream(String filePath) throws FileNotFoundException
```
…
```Java
}
```

```Java
abstract class InputStream {
```
…
```Java
  int read() throws IOException
```
…
```Java
}
```

### Implementation

```Java
String readFile(String filePath) throws FileNotFoundException,
                                        IOException {
  InputStream input = new FileInputStream(filePath);
  StringBuffer output = new StringBuffer();
  int read = input.read();
  while(read >= 0) {
    output.append((char) read);
    read = input.read();
  }
  return output.toString();
}
```

### Remark on the classes hierarchy

- `Throwable`
- $\rightarrow$ `Exception`
- $\hspace{1em}\rightarrow$ `IOException`
- $\hspace{2em}\rightarrow$ `FileNotFoundException`

. . .

```Java
String readFile(String filePath) throws IOException {
```
…
```Java
}
```

# Architecture

## Patterns

### Origins

#### Approach

- how to handle large projects ?
- experience as a programmer (c.f. chess)
- similarities from one project to the other

$\rightarrow$ programming as a *craft* vs. programming as *science*

#### A famous book


:::columns
::::{.column width=65%}

- "Design Patterns: Elements of Reusable Object-Oriented Software"
- four authors ("gang of four"), (1994)
- huge best-seller, impact on the community
- presents 23 patterns

::::
::::{.column width=25%}

\vspace{0.3cm}

![](figures/Design_Patterns_cover.jpg){height=100px}

::::
:::

### Controverse

Very praised in 1994
…then progressively criticized

- useless or redundant patterns
- attacks against the concept of "pattern" itself
	+ symptoms of "faults" in the language
	+ missing features

\vspace{1cm}

$\rightarrow$ Part of Java's history

### Singleton

- ensure one instance only
- typical use: logger
- like a global variable + lazy loading

### Singleton example

```Java
class Logger {
	private static Logger instance;
	private Logger() {
		System.out.println("Created a logger");
	}
	public static Logger getInstance() {
		if(Logger.instance == null) {
			instance = new Logger();
		}
		return Logger.instance;
	}
}

```

### Factory

- hide the constructor ("alternative constructors")
- level some internal complexity
- decouple the consumer class and the choice of a particular implementation

### Factory example: private constructors

```Java
class Sound {
	private MP3 mp3Data;
	private Ogg oggData;
	private Sound(MP3 mp3Data) {
		this.mp3Data = mp3Data;
	}
	private Sound(Ogg oggData) {
		this.oggData = oggData;
	}
```
…

### Factory example: …with a public method

```Java
	public fromFile(String path) {
		if(… == ".mp3") {
			return new Sound(new MP3(path));
		} else {
			return new Sound(new Ogg(path));
		}
	}
}
```

### Decorator

- add behaviour to a particular instance, not the whole class
- dynamic modification of functionality
- idea of "wrapping" around an existing object
- $\neq$ inheritance (runtime vs. compile-time)
- (inheritance *à la* Javascript)
- **circling around the same interface**

### Decorator example: an interface and a base class

```Java
interface Cat {
	public String getDescription();
}

class AnyCat implements Cat {
	public String getDescription() {
		return "A cat";
	}
}
```

### Decorator example: the class for decorators

```Java
abstract class CatDecorator implements Cat {
	private final Cat decoratedCat;
	public CatDecorator(Cat c) {
		this.decoratedCat = c;
	}
	public String getDescription() {
		return this.decoratedCat.getDescription();
	}
}
```

### Decorator example: and one implementation

```Java
class WithHat extends CatDecorator {
	public WithHat(Cat c) { super(c); }
	public String getDescription() {
		return super.getDescription() + " with a hat";
	}
}
```

### MVC: about

- Model — View — Controller
- even earlier than the "Design patterns" book ! (1978)
- general principle of "Separation of concerns", modularity, etc.

### Model: example

```Java
class VolumeState {
  private float volume = 0.5f;
  private boolean mute = false;
  public void changeVolume(float deltaVolume) {
    float newVolume = this.volume + deltaVolume;
    this.volume = Math.min(1, Math.max(0, newVolume));
  }
  public void toggleMute() {
    this.mute = !this.mute;
  }
  public float getVolume() {
    return this.mute ? 0 : this.volume;
  }
}
```

### Controller

```Java
class Knob {
  private VolumeState volumeState;
  public Knob (VolumeState volumeState) {
    this.volumeState = volumeState;
  }
  public void handle(String command) {
    switch(command) {
      case "+": this.volumeState.changeVolume(0.05f); break;
      case "-": this.volumeState.changeVolume(-0.05f); break;
      case "m": this.volumeState.toggleMute(); break;
      default: System.err.println("Unknown: " + command);
    }
  }
}
```

### View: example

```Java
class VolumeBar {
  private byte width;
  private VolumeState volumeState;
  private Knob knob;
  public VolumeBar(byte width, VolumeState volumeState) {
    this.width = width;
    this.volumeState = volumeState;
    this.knob = new Knob(volumeState);
  }
  …
```

### View: continued

```Java
  …
  public void run() {
    while(true) {
      display(this.volumeState.getVolume());
      this.knob.handle(System.console().readLine());
    }
  }
  public void display(float level) {
    char[] bar = new char[(int) (level * this.width)];
    for(int i = 0; i < bar.length; i++) {
      bar[i] = '#';
    }
    System.out.println(bar);
  }
}
```

# Real-world projects

## Javadoc

### Purpose

:::columns
::::{.column width=40%}

\vspace{2cm}

- normalize documentation
- make it easy to browse (HTML)
- part of the development process

::::
::::{.column width=55%}
![](figures/String_documentation.png){height=230px}
::::
:::

### Usage

:::columns
::::{.column width=45%}
```Java
/**
 * Here a general description
 * of the method
 * @param name description
 * @return description
 */
```
::::
::::{.column width=45%}
Tags

- `@param`
- `@return`
- `@author`
- `@deprecated`
- `@throws` / `@exception`
::::
:::

```sh
javadoc [PACKAGE|SOURCE-FILE]
```

(don't forget about visibility !)

### Example

```Java
/** A class exposing handy functions to generate Strings */
public class StringUtil {
	/** Replicate a String several times
	 *  @param input the initial String
	 *  @param n the number of repetitions
	 *  @return the initial String concatenated the n times */
	public static String replicate(String input, int n) {
		if(times == 0) {
			return "";
		} else {
			return input + replicate(input, times - 1);
		}
	}
}
```

### Generated files

:::columns
::::{.column width=50%}
![](figures/javadoc_generated_files.png)
::::
:::

### The main page (`index.html`)

:::columns
::::{.column}
![](figures/javadoc_main_page.png)
::::
:::

no package, and `javadoc` resents it!

### The class itself

:::columns
::::{.column}
![](figures/javadoc_class_page.png)
::::
:::

## Classpath

### In java

- projects grow
- several dependencies (libraries)

**Where to find the code ?**

. . .

current directory (`.`) but not very flexible (compilation)

. . .

#### Cross-tool concern

- `jshell`
- `javac`
- `java`
- `javadoc`
- `jar`

### From the shell

- list of "code bases": folders or `jar`
- `:`-separated ($\equiv$ `$PATH`)
- equivalent option from all the ecosystem
    + `--class-path`, `-classpath`, `-cp`
    + same syntax

```sh
echo $CLASSPATH
"path/to/some/library:path/to/another:current/project"
```

## JAR

### Archives

:::columns
::::{.column width=45%}
- contain the code
- easily distributed
- (lighter)
- independent from packages
::::
::::{.column width=45%}
#### Syntax ($\equiv$ `tar`)

- `-t`: list
- `-c`: create
- `-x`: extract
::::
:::

```sh
jar cvf MyApp.jar *.class
jar xvf MyApp.jar
jar tvf MyApp.jar | less
```

### Using libraries

*Main.java*

```Java
public class Main {
    public static void main(String[] args) {
        System.out.println(StringUtil.replicate(args[0], 3));
    }
}
```

```sh
javac Main.java
```

. . .

```
Main.java:3: error: cannot find symbol
        System.out.println(StringUtil.replicate(args[0], 3));
                           ^
  symbol:   variable StringUtil
  location: class Main
```

### Using it

assuming `../library.jar` contains `StringUtil`

```sh
javac --class-path ../library.jar Main.java
java --class-path "../library.jar:." Main test
```

Let's create a JAR

```sh
jar --create -f ../main.jar Main.class
```

we can run the app

```sh
java -cp "../library.jar:../main.jar" Main test
```
