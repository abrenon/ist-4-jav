---
title: IST-4-JAV Java Programming
subtitle: Class 3 - Modeling the structure
author: Alice BRENON `<alice.brenon@liris.cnrs.fr>`
date: 2022-10-12
institute:
	\includegraphics[height=.9cm]{figures/LIRIS}\quad
	\includegraphics[height=.9cm]{figures/INSA}
aspectratio: 169
header-includes:
	- \usepackage{fdsymbol}
	- \usepackage{textgreek}
	- \usepackage{bclogo}
	- \usepackage{Beamer/beamerthemePerso}
	- \setcounter{tocdepth}{1}
---

# Structuring objects

## Inheritance

### Object composition

**1\textsuperscript{st} approach**

- cartesian product ($\times$)
- "tie" fields together

**Example**

Modeling a (video game) cat:

- age: a very small integer
- hit points: some small integer
- max hit points: some small integer
- rest level: decimal number $\in [0, 1]$

### First draft of a Cat

```Java
class Cat {
	byte age;
	short hitPoints;
	short maxHitPoints;
	float restLevel;
	static short babyMaxHitPoints = 3;
	Cat() {
		this.age = 0;
		this.maxHitPoints = babyMaxHitPoints;
		this.hitPoints = this.maxHitPoints;
		this.restLevel = 1;
	}
}
```

### How about domestic cats ?

$\rightarrow$ take a `Cat`, and add a name to it ?

```Java
class DomesticCat {
	Cat innerCat;
	String name;
	DomesticCat(String name) {
		this.name = name;
		this.innerCat = new Cat();
	}
}
```

### Not everything is composition !

- `innerCat` ? sounds bad
- we'll eventually add methods: `feed`, `run`…: how will it work for our
  `DomesticCat` ?

```Java
void feed() {
	this.innerCat.feed()
}
void run() {
	this.innerCat.run();
}
```

$\implies$ with this definition a `DomesticCat` is *not* a `Cat`

### Inheriting

**conceptually differs from composition**

- distinguish a "special case"
- useful when both classes model the same "kind" of real-world objects
- "A *is* a B"


$\implies$ perfect for our cats !

**Programming: models for structures and behaviours, not just data**

### Example: Object

inheritance is not rare:

- any class has exactly 1 *parent*/*mother*/*superclass*
- except `Object` ! sits at the bottom
- actually… always inherits at least `Object`

![Classes form a tree](graphs/classesTree.png){height=120px}

### Example: Exceptions

:::columns
::::{.column width=60%}
![A taxonomy of trouble](graphs/exceptionsTree.png)

::::
::::{.column width=40%}
- `Throwable`: anything that can be `throw`n
- `Error`: serious "physical" trouble
- `Exception`: recoverable failure
- `TimeoutException`: give up on blocking operation
- `NullPointerException`: tried to dereference `null`
::::
:::

### Syntax: `extends`

```Java
class DomesticCat extends Cat {
	String name;
	DomesticCat(String name) {
		this.name = name;
	}
}
```

declares a new class `DomesticCat` which *inherits* `Cat`: it has everything a
`Cat` has !

```Java
DomesticCat pangur = new DomesticCat("Pangur");
pangur.name; // == "Pangur"
pangur.age; // == 0
pangur.restLevel; // == 1.0
```

### Wait ?!

- `pangur.name` got initialized: ok, we did that
- `pangur.age` and `pangur.restLevel` got initialized: why ? also how ?

**magic happened**: Java called the mother class constructor !

but what would've happened if an argument was needed… ?

## Mother/child relationships

### Yet another inheritance !

People start caring about the *breed* of their cats:

```Java
class PureBreedCat extends DomesticCat {
	String breed;
	PureBreedCat(String breed) {
		this.breed = breed;
	}
}
```

\tiny\begin{verbatim}
|  Error:
|  constructor DomesticCat in class DomesticCat cannot be applied to given types;
|    required: java.lang.String
|    found:    no arguments
|    reason: actual and formal argument lists differ in length
\end{verbatim}
\normalsize

### A hidden call !

```Java
new DomesticCat()
```

\tiny\begin{verbatim}
|  Error:
|  constructor DomesticCat in class DomesticCat cannot be applied to given types;
|    required: java.lang.String
|    found:    no arguments
|    reason: actual and formal argument lists differ in length
|  new DomesticCat()
\end{verbatim}
\normalsize

$\rightarrow$ there was a (hidden) call to  `DomesticCat();`

### Calling the constructor

- the automatically added call takes no argument
- `DomesticCat` doesn't have a constructor with no argument !
- we need to manually call the constructor "above"

```Java
class PureBreedCat extends DomesticCat {
	String breed;
	PureBreedCat(String name, String breed) {
		super(name);
		this.breed = breed;
	}
}
```

### `super`

- *meta* keyword, refer to a class
- depending on the context ! ($\|$ `this`)
- "the parent class"

**Look !**: call to a function, not a method

### Fixed it !

```Java
PureBreedCat billy = new PureBreedCat("Billy", "siamese");
billy.name; // == "Billy"
billy.breed; // == "siamese"
billy.age; // == 0
billy.restLevel; // == 1.0
```

$\rightarrow$ now we know where everything comes from !

### Overriding

- inheriting methods is good
- but sometimes we want to change their behaviour
- `@Override` pragma: hint for `javac`

:::columns
::::{.column width=45%}
```Java
class Cat {
	…
	void feed() {
		System.out.println(
			"hunts"
		);
	}
}
```

::::
::::{.column width=45%}
```Java
class DomesticCat extends Cat {
	…
	@Override
	void feed() {
		System.out.println(
			"mews until a human"
			+ " feeds it"
		);
	}
}
```
::::
:::

### Another use for super

- redeclaring a method in a child class "hides" the parent's implementation
- how to reuse the existing implementation ?
- `super` again !

```Java
class DomesticCat extends Cat {
	…
	void feed() {
		if(isHumanAround()) {
			System.out.println("mews a lot");
		} else {
			super.feed();
		}
	}
}
```

### Example: Built-in methods

defined in `Object` (so all objects in Java have them)

- `.equals`: object comparison
- `.toString`: object representation
- `.hashCode`: at-a-glance comparison (fast, vs. accurate `.equals`)

$\rightarrow$ all *default* implementations meant to be overridden

```jshell
jshell> pangur
pangur ==> DomesticCat@723279cf
```

default `.toString`: *Class name* + `@` + *memory location*

### A nicer display

```Java
class DomesticCat extends Cat {
	String toString() {
		return this.name + ", a cat";
	}
}
```

\vspace{-0.3cm}

```Java
class PureBreedCat extends DomesticCat {
	String toString() {
		return this.name + ", a " + this.breed + " cat";
	}
}
```

\vspace{-0.3cm}

```jshell
jshell> pangur
pangur => Pangur, a cat
jshell> billy
billy => Billy, a siamese cat
```

### Overloading

#### Signature

- the number of arguments
- their order
- their type

#### Added, not replaced

Java finds functions by their name **and** signature

- classes can have several constructors
- methods can have several implementations
- they can have different *output* types
- the arguments + name must be unique (can't rely on *output*)

### Example: feeding

(assuming we never wrote the previous implementation with `super`)

```Java
class DomesticCat extends Cat {
	void feed(Human h) {
		System.out.println("mews until human feeds it");
	}
}
```

- doesn't replace the default implementation in `Cat` (hunting)
- in the context where a human is passed, mews instead
- a "conditional" without `if` or ternary operator.

# Structuring code

## Access control

### Example: a cat's `name`

can't ask its name to a cat !

. . .

*but*

```Java
pangur.name; // returns a String ("Pangur")
pangur.name = "Marcel"; /* now Pangur has a different name ! */
pangur.name; // "Marcel"
```

### The `private` keyword

```Java
class DomesticCat extends Cat {
	private String name;

	// but still can interact with it !
	void call(String name) {
		if(this.name.equals(name)) {
			System.out.println("mews and comes");
		} else {
			System.out.println(
				"looks away and yawns"
			);
		}
	}
}
```

### Why ?

- more realistic
- hides away implementation details
- avoid mistakes
- classes as an "area"
- (remember "structure", not "data" ?)

$\rightarrow$ abstraction


### Another useful flag: `final`

:::columns
::::{.column width=45%}
```java
Math.PI
Math.E
```
::::
::::{.column width=10%}
$\rightarrow$
::::
::::{.column width=35%}
```java
static final double
```
::::
:::

- prevent from changing a variable
- the variable itself (not what it may refer to ! remember graphs)
- again: numbers, objects…

#### Not for variables only

- **On methods**: cannot be overridden
- **On class**: cannot be extended

### Getters

- by default variables are read-write
- read-only can be achieved by `final`
- "access control": actually read-write, but can't be changed from *outside*
- notion of *view* (there doesn't have to be a corresponding field)

```Java
class Cat {
	private int age;
	int getAge() {
		return this.age;
	}
}
```

### Setters

- useful even for read-write !
- separates feature / implementation

```Java
class Cat {
	private int age;
	void setAge(int age) {
		if(age > this.age) {
			this.age = age;
		}
	}

	void happyBirthday() {
		this.age++;
	}
}
```

### Advanced setters

- side-effects
- control the flow

```Java
class MovingAnimal {
	private float restLevel;
	private Point at;
	static float range;
	void setAt(Point newAt) {
		float distance = distance(this.at, newAt);
		this.at = newAt;
		this.restLevel *= Math.exp(-distance / range);
	}
}
```

## Building blocks

### Abstract classes and methods

*What's a **predator** ?*

. . .

- the Idea without a Form
- implements some behaviour
- can't be instantiated, a "draft"
- exists only to be inherited (*factorize* code)

### Syntax: `abstract`

- on the class itself
- on the methods without implementation
- incompatible with `final` !

```Java
abstract class Predator {
	abstract protected void catchPrey();
	protected void feed() {
		this.catchPrey();
		System.out.println("eats it");
	}
}
```

### Interfaces

- a "contract": requirements
- a "definition" ($\rightarrow$ flexible "type" for classes)
- not meant for instantiation either
- variables are all `static` and `final`

**almost *opposite* of abstract classes**

:::columns
::::column
#### Abstract classes

- partial implementation (delay a "choice")
- really a class (inherited like any other)
- (only one mother class, `abstract` or not)

::::
::::column
#### Interfaces

- what, not how
- not inherited, *implemented*
- (no restriction on number)

::::
:::

### Syntax: `interface` then `implements`

:::columns
::::column

```Java
interface Animal {
	int getAge();
	void feed();
}
```

::::
::::column

\vspace{0.5cm}

"We call `Animal` any object which…"

::::
:::

\vspace{1cm}

:::columns
::::column

```Java
class Cow implements Animal {
	private int age;
	int getAge() {
		return this.age;
	}
	void feed() {
		System.out.println("grazes");
	}
}
```

::::
::::column

\vspace{1cm}

"A cow is an Animal, and here's the proof"

::::
:::

### Bridges between interfaces and abstract classes

#### Extending

- an `interface` can be inherited (`extends`, same syntax)
- sort of prerequisites, set inclusion: all birds are animals

#### Partial implementation

- a class may `implement` an interface but not all its methods
- makes unimplemented `abstract` $\implies$ whole class `abstract`

#### Default implementation

- maybe confusing ($\approx$ regular methods in `abstract`)
- perfect use case: augmenting interface backwards-compatibly

## What about typing ?

### Subtype vs. Derived type

:::columns
::::column

#### Subtype

- substitutability
- `T1` $\prec$ `T2`
- `T1` can replace `T2` everywhere
- ex: `int` $\prec$ `long`

::::
::::column

#### Derived type

- inheritance (everything above)
- "special case"

::::
:::

\begin{center}
subtype $\overset{?}\iff$ derived type
\end{center}

### Covariance / Contravariance

assuming `A` $\prec$ `B`

\vspace{1cm}

:::columns
::::column

$\rightarrow$

- if I *have* an `A`
- then I have a `B`
- covariance

::::
::::column

$\leftarrow$

- if I *need* an `A` 
- a `B` might not be enough
- contravariance

::::
:::

\vspace{1cm}

- functions: left or right of $\mapsto$ ?
- programs as games: "whose turn ?"

**Contravariance reverses subtyping**

### Answer

- objects have methods
- methods are functions
- hence, can be contravariant

```Java
class Cat {
	Cat mate(Cat partner) {
		…
	}
}

class PureBreedCat extends DomesticCat {
	PureBreedCat mate(PureBreedCat partner) {
		…
	}
}
```

**$\rightarrow$ Not in general !**

# Structuring projects

## Isolating parts

### Packages

- applications start growing
- avoid name conflicts
- structuring things also document them
- reuse some parts
- developed by different organisations

$\rightarrow$ packages

### Creating: `package`

:::columns
::::column

```Java
package name.of.the.package;
```
::::
::::column

statement must be added to each file

::::
:::

- arbitrary names (usually "company"'s domain name)
- hierarchically left to right (opposite from internet domain names)
- all lowercase
- use `_` to fix invalid names (reserved words, `-` and other special
  characters)

### Virtual Path / Filesystem Path

- path to files should reflect packages hierachy
- lowercase for packages $\rightarrow$ directory
- CamelCase for classes $\rightarrow$ files

![Two example packages](figures/packages.png){width=400px}

### Using: `import`

```Java
import name.of.the.package.SomeClass; // one class
import name.of.the.package.*; // all its classes
```

\vspace{1cm}

- you can only import classes, not packages
- no "subpackages": prefix on names $\not \Rightarrow$ anything on package
- `*` isn't a regex, and (see above) will catch only classes

## More about visibility

### `public`, at last !

- control the boundaries at the package level
- default: nothing outside the package
- (`jshell`: same temporary package)

\vspace{1cm}

:::columns
::::{.column width=45%}
`public`: entirely visible
::::
::::{.column width=45%}
`protected`: only in subclasses ("backstage" access)
::::
:::

### How ?

- modifiers applied to
	+ fields
	+ methods
	+ classes

#### Keywords / visibility

\vspace{0.5cm}
\begin{tabular}{| l | c | c | c | c |}
\hline
	keyword & class & package & subclass & outside world\\
\hline
\hline
	$\texttt{private}$ & Y & N & N & N\\
\hline
	(nothing) & Y & Y & N & N\\
\hline
	$\texttt{protected}$ & Y & Y & Y & N\\
\hline
	$\texttt{public}$ & Y & Y & Y & Y\\
\hline
\end{tabular}

### Good practice

"Need to know" basis $\implies$ start `private`, and grant access as needed

#### Model the reality

- who/what could get the information ?
- could it be changed from *outside* ?
- is it a convenience or something fundamental ?

### A subtle remark

- `private` is very restrictive (the class to itself)
- $\rightarrow$ makes sense only *inside* a class

. . .

- `protected` differs from default only for inheritance
- $\rightarrow$ makes sense only *inside* a class

## Example

### Source code

*fr/insa_lyon/ist_4_jav/library/SomeLibrary.java*

```Java
package fr.insa_lyon.ist_4_jav.library;

public class SomeLibrary {
	public static String greet = "Hi there !";
}
```

### Source code

*fr/insa_lyon/ist_4_jav/class3/Main.java*

```Java
package fr.insa_lyon.ist_4_jav.class3;
import fr.insa_lyon.ist_4_jav.library.SomeLibrary;

public class Main {
	public static void main(String[] args) {
		System.out.println(SomeLibrary.greet);
	}
}
```

### Compiling

- `javac` works on files
- `java` works on class names
- (use autocomplete to let `java` guide you)

```shell
$ javac fr/insa_lyon/ist_4_jav/class3/Main.java
$ java fr.insa_lyon.ist_4_jav.class3.Main
Hi there !
```

