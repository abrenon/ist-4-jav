---
title: IST-4-JAV Java Programming
subtitle: Class 1 — (re ?)Discovering Java
author: Alice BRENON `<alice.brenon@liris.cnrs.fr>`
date: 2023-01-02
institute:
	\includegraphics[height=.9cm]{figures/LIRIS}\quad
	\includegraphics[height=.9cm]{figures/INSA}
header-includes:
	- \setcounter{tocdepth}{1}
	- \usepackage{fdsymbol}
	- \usepackage{textgreek}
	- \usepackage{bclogo}
	- \usepackage{Beamer/beamerthemePerso}
---

## Foreword: the IST-4-JAV course {-}

### Timetable

#### 20h: 5×4h-sessions

- 2023-10-02 8 a.m. (today!)
- 2023-10-04 2 p.m.
- 2023-10-06 8 a.m.
- 2023-10-11 2 p.m.
- 2023-10-13 8 a.m.

- (one class every other day this week, same last week except monday)

#### Time repartition

- ~2h course
- (a break in-between)
- ~2h practice

### Course home

[https://perso.liris.cnrs.fr/abrenon/IST-4-JAV.html](https://perso.liris.cnrs.fr/abrenon/IST-4-JAV.html)

### How to pass this class ?

**Evaluation**

Game project demonstrating the object-oriented concepts seen in class

#### Time-budget

- 20h together in class
- 20h at home
	+ 1h½ after each class
	+ 12h½ project

---

\tableofcontents

# About programming

## Modeling things

### Back to maths 101

:::columns
::::{.column width=35%}
What is a *number* ?
::::
:::

. . .

:::columns
::::{.column width=55%}
Let's consider: **3**

- $\triangle$ (geometric property) ?

. . .

- $\{a, b, c\}$ (set) ?

. . .

- $succ(succ(succ(0)))$ (Peano arithmetic) ?

. . .

- $\frac{51}{17}$ (result of a computation) ?

. . .

:::
::::{.column width=40%}
**Base b**

- `11` (base 2)

. .  .

- `3` (base 13)

. . .

$$\sum_{i=0}^{\infty}{c_i * b^i}$$

::::
:::

### Non-positional systems

:::columns
::::column

![Glagolitic numerals (1280)](figures/glagolitic.jpg){height=60px}

::::
::::column

![Roman numerals (1909)](figures/roman-mcmix.jpg){height=60px}

::::
:::

\begin{align*}
I + II & = III & \text{easy!}\\
I + IV & = V & \text{uh ?}\\
XCV + V & = C & \text{haha good one romans \^{}\^{}}
\end{align*}

- hard to write addition rules
- not digits, numbers ('X' $\nequal$ 'C' vs. '1' in 10 $\equal$ '1'in 100)
- limited (no symbol > 1000)


### The advantage of positional systems


:::columns
::::column

- finite set of simple ("mechanical") rules
- can represent any number, even one you've never even considered

:::
::::column

$$132 + 41 = ??$$

. . .

\begin{center}
…in base 5!
\end{center}

::::
:::

### Back to elementary school!

\begin{center}
\begin{tabular}{c | c c c c c}
+ & 0 & 1 & 2 & 3 & 4\\
\hline
0 & 0 & 1 & 2 & 3 & 4\\
1 & 1 & 2 & 3 & 4 & $\overset{+1}{}$ 0 \\
2 & 2 & 3 & 4 & $\overset{+1}{}$ 0 & $\overset{+1}{}$ 1 \\
3 & 3 & 4 & $\overset{+1}{}$ 0 & $\overset{+1}{}$ 1 & $\overset{+1}{}$ 2\\
4 & 4 & $\overset{+1}{}$ 0 & $\overset{+1}{}$ 1 & $\overset{+1}{}$ 2 & $\overset{+1}{}$ 3\\
\end{tabular}
\end{center}

\vspace{1cm}

:::columns
::::{.column width="25%"}
\begin{tabular}{c@{\,}c@{\,}c@{\,}c}
  & &  &  \\
  & 1 & 3 & 2 \\
+ &   & 4 & 1 \\
\hline
\end{tabular}

::::

. . .

::::{.column width="25%"}
\begin{tabular}{c@{\,}c@{\,}c@{\,}c}
  & &  &  \\
  & 1 & 3 & 2 \\
+ &   & 4 & 1 \\
\hline
  &  &  & 3 \\
\end{tabular}

::::

. . .

::::{.column width="25%"}
\begin{tabular}{c@{\,}c@{\,}c@{\,}c}
  & $\overset{1}{}$ &  &  \\
  & 1 & 3 & 2 \\
+ &   & 4 & 1 \\
\hline
  &  & 2 & 3 \\
\end{tabular}

::::

. . .

::::{.column width="25%"}
\begin{tabular}{c@{\,}c@{\,}c@{\,}c}
  & $\overset{1}{}$ &  &  \\
  & 1 & 3 & 2 \\
+ &   & 4 & 1 \\
\hline
  & 2 & 2 & 3 \\
\end{tabular}

::::
:::

### Let's check!

\begin{align*}
132 & = 2*5^0 + 3*5^1 + 1*5^2 = 2 + 15 + 25 = 42 \\
41 & = 1*5^0 + 4*5^1 = 1 + 20 = 21 \\
223 & = 3*5^0 + 2*5^1 + 2*5^2 = 3 + 10 + 50 = 63
\end{align*}

\begin{center}
$\medbackslash\,\hat{}$ $\theta$ $\hat{}\,\medslash$
\end{center}

### A *model* of numbers

- a concept: *numbers*
- a representation: *digits*
- arithmetic rules to handle digits
- $\rightarrow$ know how to write a number with digits: *encode*
- $\leftarrow$ know what number digits represent: *decode*

### General pattern

\begin{center}
\only<1>{\includegraphics[height=50px]{graphs/parallelogram0.png}}
\only<2>{\includegraphics[height=50px]{graphs/parallelogram1.png}}
\only<3>{\includegraphics[height=50px]{graphs/parallelogram2.png}}
\only<4>{\includegraphics[height=50px]{graphs/parallelogram3.png}}
\end{center}

### The core of programming

- defining abstract concepts from a concrete implementation (the **right**
  level)
	+ too complex: it's slow
	+ too simple: it's hard to use
- solve problems using the abstraction
- have a system translate it to the implementation
- repeat
- run

### Layers

:::columns
::::{.column width=35%}
- higher-level languages
- C
- assembly
- machine binary
::::
::::{.column width=55%}
\vspace{1cm}
$\updownarrow$ compilation / interpretation
::::
:::
## Expressing computations

### Imperative

:::columns
::::{.column width=45%}
- "do things in a given order"
- recipe
- implicit reference to a state
::::
::::{.column width=50%}

. . .

```C
for(int i = 0; i < 4; i++) {
    a[i] += 1;
}
```
::::
:::

### Functional

:::columns
::::{.column width=45%}
- "describe the computation itself"
- based on lambda-calculus
- everything is a function ($\Rightarrow$ higher-order)
::::
::::{.column width=35%}

. . .

```haskell
fmap (+1) positions
```
::::
:::

### Object

:::columns
::::{.column width=45%}
- "as a metaphor of a physical object"
- associate data and logic
- explicit reference to an identified state
::::
::::{.column width=45%}
```java
for(Cell cell : cells) {
    cell.incr();
}
```
::::
:::

### Anything else ?

. . .

- logic:

```
sum(s(a), b) :- sum(a, s(b))
```
. . .

- concatenative:
```
: fac 1 swap 1+ 1 ?do i * loop ;
```
. . .

- (machine learning ?)

### Compiling vs. Interpreting

:::columns
::::column
**Compiler**

- generate low-level from high-level
- optimized, fast
::::
::::column
**Interpreter**

- translated on the fly (duality program / data)
- generally slower (+ loading time)
- portable!
::::
:::

### Typing

*Labels* on things in the memory:

- "strong" or "weak"
- explicit or implicit
- static or runtime
- more or less expressive
    + `void*`
    + (G)ADT
    + entire logic system

## Actually running them

### A finite memory

- hopefully "big enough"
- representing *numbers*

. . .

:::columns
:::{.column width=30%}
![An abacus](figures/soroban.jpg)

::::
::::{.column width=30%}
\vspace{1cm}
![A modern Pascal's calculator](figures/pascaline.jpg)

::::
::::{.column width=30%}
![The Analytical Engine](figures/analytical_engine.jpg)

::::
:::

. . .

- which can represent *things*

### Anything is a number

#### Directly ("native")
- **Truth value**
True or False, 2 values $\rightarrow$ 1, 0

- **Numbers**
obvious **but** overflow

- **Characters**
	+ very natural (remember non-positional systems ?)
	+ known for very long (before Cæsar)!
	+ $\rightarrow$ encodings (ASCII, UTF-8…)

#### As a sequence
- notion of address
- special strategies: "stop" symbol vs. length
- "large" numbers
- text
- multimedia

### The right word

An *atomic* number $\doteq$ a *word*

**How to choose the right bits size**

- too large is painful to build
- too small is painful to use (slow)

### Instructions

- the **paths** in the circuit
- at the **electrical** level
- **hardwired** operations
	+ sum
	+ multiplication
	+ bit shift
	+ xor
	+ …

### Architecture

word size + set of instructions = a "machine"

**Examples**

- x86\_64
- arm64
- i686
- riscv64

### Virtual architecture

- data: *numbers*
- operations: *numbers*

$\Rightarrow$ we can have *programs* pretending to be *machines* (see Turing machines)

## Java

### Concepts

**Compiled or interpreted ?**

- compiles to a binary: *bytecode*…
- but for a *virtual machine*! "JVM"
- "Write Once Run Anywhere"

**Features**

- Object-Oriented (+ Imperative)
- strictly typed: forget that "anything is a number"
- rich collection of built-ins for data structures, I/O…
- automatic memory handling (garbage collector)

### History

**Context**

- released in 1995 (32-bits architectures)
- after the Eternal September!

**Internet oriented**

- support from Netscape
- the .com frenzy
- applets, "servlets" (e-commerce, administrations…)

**(Big) Business**

- created by Sun Microsystem, bought by Oracle
- IDE (NetBeans, Eclipse), "easy", "predictible" (developer as a "worker")
- a Java "Enterprise Edition" (vs. JSE)
- a lot of marketing, "Java" meant "cool" ($\rightarrow$ "javascript")
- $\rightarrow$ second life in mobile / handheld

# Language basics

Contains real bits of Java.

- `this` is for actual valid code
- `<THIS>` is for meta bits of code (templating)
- mind the case, the quotes, etc.

## Types

### What they are

How to navigate the "everything is a number" soup ?

. . .

$\rightarrow$ *flags*

- boundaries (size)
- purpose, intention
- $\sim$ sets in maths
- prevent (some) errors

### Usage

#### Convention

- native (lowercase)
- sequences (uppercase-first)
- `void`

#### Every value or function

must be annotated with its type (Java does it too and compares)

### A REPL!

In `jshell`[^1] (Read - Eval - Print Loop) type

- */vars* : to print the known variables with their type
- */set feedback verbose* : to include the type of expressions in the evaluation
  output

[^1]: [https://tryjshell.org/](https://tryjshell.org/)

This is **not Java**! only special commands for the interpreter

## Data structures

### "native" numbers

:::columns
::::{.column width=100%}
#### Truth values

- `boolean` useful for conditionals
::::
:::

**Examples**

- `true`
- `false`

---

:::columns
::::{.column width=100%}
#### Integers

- `byte` 8 bits integers $\rightarrow [-128,127]$
- `int` 32 bits integers $\rightarrow [-2147483648,2147483647]$
- `short` 16 bits integers $\rightarrow [-32768,32767]$
- `long` 64 bits integers $\rightarrow [-2^{63},2^{63}]$
::::
:::

**Examples**

- `0`
- `-1`
- `1347` (not for byte)
- `0b11` (binary), `046` (octal), `0xa3` (hexadecimal)

---

:::columns
::::{.column width=100%}
#### Decimal numbers

- `float` 32 bits decimal numbers, scientific notation, significand/exponent
- `double` same with 64 bits (+ precision)
::::
:::

**Examples**

- the previous (since $\mathbb{N} \subset \mathbb{R}$)
- `1.03`, `-0.47`, `320.`
- `314e-2`, `-1.21E7`
- `1f`, `2.0d`, `-1.237e12F`

---

:::columns
::::{.column width=100%}
#### Unicode characters

- `char` 16 bits, UTF-16, written between simple quotes
::::
:::

**Examples**

- `'a'`, `'b'`, `'0'`, `'!'`, `'@'`, `'é'`…
- `'\n'`, `'\r'`, `'\t'`…
- `'\''`, `'\\'`
- `'\u00e9'` (code point)
- `10`, `0x27`

### "sequences" $\equal$ objects

:::columns
::::{.column width=100%}
#### "Arbitrary" precision

- `BigInteger` large integers
- `BigDecimal` large decimal numbers

#### Text

- `String` immutable sequences of characters
::::
:::

**Examples**

- `""`
- `"Some text"`
- `"first line\nsecond line\n"`

## Values

### Constants

- (everything we've just seen)
- "magic" values, not all explicitly defined (because: `long`, `String`)
- a very special constant only for objects: `null`

\vspace{1cm}

#### Problems with constants

- "Don't Repeat Yourself"
- they don't carry any *intention* ("beware of names" said I!!)
- "constants" change sometimes (e.g. exchange rate)

### Variables

#### Concept

- give a **meaningful** name to a value
- absolutely abstract, will need to refer to a place in the memory
- a "wire", "bringing" the value (*no copy*)

#### Valid names

- must start by: a letter (`[a-zA-Z]`), `$` or `_`
- may contain any above and digits `[0-9]`
- except reserved keywords: `if`, `else`, `for`, `while`, `return`, `try`, `catch`, `static`, `final`…
- usually: full uppercase for "constant" variables, camelCase for the rest

### Built-in (`final`) variables

- streams: `System.in`, `System.out`
- maths: `Math.PI`, `Math.E`

## Comments

- clarify intent
- not a remedy for bad naming
- not needed to caption the obvious
- can contain the documentation (*javadoc*)

### Syntax

**Rest of line**

```java
// this is a comment
4 // it doesn't have to start with the line
// but it ends it so this is not a number: 17
```

**General comments**

```java
/* these comment can start on a line
   and end on another one */
/* Since they have an end, this is a number: */ 4
/** two asterisks like this for a Javadoc string */
```

## Functions and Procedures (built-ins only today)

### Functions

- abstracts a computation by isolating its inputs
- name it ("beware of names"!)
- has several input types (for its arguments) and one output type (for its
  result)
- its arguments are written within parentheses (both in declaration and call)

#### Built-in functions examples

- **type conversions** \texttt{Integer.parseInt}, \texttt{Integer.toString}…
- **maths toolbox** \texttt{Math.max}, \texttt{Math.min}, \texttt{Math.exp}…
- …

### Procedures

- simply "do" something
- no result
- (actually has special output "type" `void`)

#### Built-in procedures

- `Thread.sleep` (pause execution)
- `System.exit` (quit the program)

### Operators (all built-ins)

- special functions with an infix notation
- name: a few punctuation/typographic characters

#### Unary

- `~`, `!`

#### Binary

- numbers: `*`, `-`, `/`, `%`
- numbers and `String`s: `+`
- boolean: `||`, `&&`
- bitwise: `|`, `&`, `>>`, `<<`, `^`
- comparisons: `==`, `!=`, `>`,  `<`, `>=`, `<=`

### Methods

- functions or procedures
- tied to an object by a `.`
- names need not be unique

**Examples**

- `System.out.println`
- `"some string".length`
- `userName.charAt`
- `password.equals`

### About the memory

**Value is just a (JVM, not physical) `word`**

- "Numbers" $\rightarrow$ their direct value
- objects $\rightarrow$ address in memory
- (can be nested, so it's just a graph of pointers)
- no direct access but: binary operators, variables

**Consequences**

- only objects can be `null`
- but **`null` isn't an object**
- `==` / `!=` on objects compare addresses

## Expressions and Statements

### Simple bricks ("atoms")

#### Expressions

- compute a value
- constants
- variables (in a context where they are defined — "plugged wire")
- any other type

#### Statements

- do something (change state)
- only "atomic" statement: variable declaration
- type `void`

---

:::columns
::::{.column width=100%}
#### Declaring a variable

- reserve space in memory
- tag it with a given type
- can be set with an initial value, but always initialized
	+ "numbers": to the equivalent of `0`
	+ objects: to `null`
::::
:::

**Examples**

```java
int messageLength;
String userName;
char firstLetter = 'a';
```

### Nesting

:::columns
::::{.column width=100%}
#### Expressions

- if `e` is an expression, so is `(e)`
- (useful for operators priority)

#### Statements

- if `s1` and `s2` are statements, `s1; s2` is a statement
- semantics: `s1` then `s2`
- in practice, wrap all the list within `{`…`}`
::::
:::

```java
{ s1; s2; s3; … ; sn }
```

### Casts

:::columns
::::{.column width=100%}
#### Use

- force a conversion between types
- may lose information (beware of truncation)
- (give hints to Java)
::::
:::

assuming

- `t` is a type
- `<VALUE>` is an expression

```java
(t) <VALUE>
```

is an **expression** of type `t` with value "projected" from `<VALUE>`

### Casts examples

```java
(short) 4 // still 4, but coded on 16 bits
(byte) 'c' // == 99
(float) (2.5 / 2) // still 1.25, but as a float
(int) 7.2 // truncates to 7
(int) 7.9 // 7, it truncates and doesn't round
(float) ((int) 7.2) // still 7.0, information was
                    // lost
```

### Function application

- a call to a function or an operator is an **expression**
- a call to a procedure is a **statement**

:::columns
::::{.column width=100%}
#### Syntax
- function or procedure: its name followed by the comma-separated arguments
  between parentheses
- operators: the symbol before, between or after its argument(s)
::::
:::

**Examples**

```java
!false
total / count
"Hello, " + "world!"
Math.pow(Math.E, -1)
System.out.println(someMessage);
```

### Variable assignment

an expression which

- changes a value in the memory
- returns a value

assuming

- `a` is a variable of type `t`
- `<VALUE>` is an expression of type `t`

```java
a = <VALUE>
```

is an **expression** wich assigns the value of `<VALUE>` to `a` and has the same value

### Assignment operators

- **Binary** the previous binary (except `boolean`) operators followed by `=`
  (`+=`, `*=`, `/=`, `|=`, `<<=`…)
- **Unary**
	+ shortcuts for the pattern `a = a · 1` (· : '+' or '-')
	+ *increment*: `++` / *decrement*: `--`
	+ before or after the variable: value after or before the change

```java
int a = 4;
++a; // a is now 5, the line evaluates to 5
--a; // a is back to 4, the line evaluates to 4
int b = a++; // read then increment: a is now 5
			 // b is 4
a--; // a is back to 4 again, the line evaluates
     // to 5
```

## Control structures 101

### Conditional statement

assuming

- `<TEST>` is an expression of type `boolean`
- `<WHEN_TRUE>` and `<WHEN_FALSE>` are statements

:::columns
::::column

```java
if(<TEST>) {
	<WHEN_TRUE>
} else {
	<WHEN_FALSE>
}
```

::::
::::column

```java
if(<TEST>) {
	<WHEN_TRUE>
}
```

::::
:::

are **statements** which:

- if `<TEST>` evaluates to `true`, execute `<WHEN_TRUE>` 
- otherwise execute `<WHEN_FALSE>` (or nothing for the shorter form)

### Conditional expression

assuming

- `<TEST>` is an expression of type `boolean`
- `<WHEN_TRUE>` and `<WHEN_FALSE>` are expressions

```java
<TEST> ? <WHEN_TRUE> : <WHEN_FALSE>
```

is an **expression** which value is:

- `<WHEN_TRUE>` if `<TEST>` evaluates to `true`
- `<WHEN_FALSE>` otherwise

### `for` loops

assuming

- `<INITIALIZE>`, `<ITERATE>` and `<BLOCK>` are statements
- `<TEST>` is an expression of type `boolean`

```java
for(<INITIALIZE>; <TEST>; <ITERATE>) {
	<BLOCK>
}
```

is a **statement** which:

- executes `<INITIALIZE>`
- if `<TEST>` evaluates to `true`, executes `<BLOCK>` then `<ITERATE>` then
  start over from this line
- otherwise stops

---

### `while` loops

assuming

- `<TEST>` is an expression of type `boolean`
- `<BLOCK>` is a statement

```java
while(<TEST>) {
	<BLOCK>
}
```

is a **statement** which:

- if `<TEST>` evaluates to `true`, executes `<BLOCK>` and start over from this
  line
- otherwise stops

# Using it

## How things work in general

### Reminder

- hybrid between compiled and interpreted
- compiles ("source") code to binary
- binary is not for the physical architecture but for the Java Virtual Machine

### Java projects

- organized in packages (folders)
- packages contain classes (files)
- classes contain "code"
	+ values
	+ methods

### Names

:::columns
::::{.column width=45%}
#### The Operating System

- file system path
- separated by `/`
- `.java` extension
::::
::::{.column width=45%}
#### Java

- package / classes
- separated by `.`
- no extension
::::
:::

### Compilation

**Operating System** $\rightarrow$ **Java**

- `static`
- `.java` extension
- compiled into bytecode `.class`
- may catch some mistakes

```
javac [OPTIONS] PATH_TO_SOURCE_FILE
```

### Execution

- `dynamic`, `runtime`
- the `.class` run by the JVM
- may catch some other errors

```sh
java [OPTIONS] PATH_TO_BYTECODE
```

## For today

### Read the documentation

#### Main page of the official Java documentation

[https://docs.oracle.com/en/java/javase/19/docs/api/index.html](https://docs.oracle.com/en/java/javase/19/docs/api/index.html)

#### Some particularly interesting modules and packages to start

- [`java.base`](https://docs.oracle.com/en/java/javase/19/docs/api/java.base/module-summary.html)
- [`java.lang`](https://docs.oracle.com/en/java/javase/19/docs/api/java.base/java/lang/package-summary.html)

### Example

[String](https://docs.oracle.com/en/java/javase/19/docs/api/java.base/java/lang/String.html)

![](figures/String_documentation.png)

### JShell documentation

[https://cr.openjdk.org/~rfield/tutorial/JShellTutorial.html](https://cr.openjdk.org/~rfield/tutorial/JShellTutorial.html)

### Time for practice : ) {-}

[https://perso.liris.cnrs.fr/abrenon/IST-4-JAV.html](https://perso.liris.cnrs.fr/abrenon/IST-4-JAV.html)
