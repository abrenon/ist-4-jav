---
title: IST-4-JAV Java Programming
subtitle: Class 4 - Going graphic
author: Alice BRENON `<alice.brenon@liris.cnrs.fr>`
date: 2022-10-19
institute:
	\includegraphics[height=.9cm]{figures/LIRIS}\quad
	\includegraphics[height=.9cm]{figures/INSA}
aspectratio: 169
header-includes:
	- \usepackage{fdsymbol}
	- \usepackage{textgreek}
	- \usepackage{bclogo}
	- \usepackage{Beamer/beamerthemePerso}
	- \setcounter{tocdepth}{1}
---


# Complexity

## Simple containers

### Implementing interfaces

![A common structure](graphs/implementations.png){height=80px}

One interface, several implementations.

### Two implementations

:::columns
::::{.column width=90%}
#### List

- a set of functionalities
- a contract to prove a class can act as a list

. . .

$\rightarrow$ an interface!
::::
:::

\vspace{0.5cm}

:::columns
::::column

**ArrayList**

\vspace{0.5cm}

![](graphs/arrayList.png)

- an array larger than the number of elements
- an index to remember where it stops

::::
::::column

**LinkedList**

\vspace{0.5cm}

![](graphs/linkedList.png)

- a "head": the element in the current cell
- a "queue": the pointer to the rest of the list

::::
:::

### Getting element at index `i`

:::columns
::::column

**ArrayList**

\vspace{0.5cm}

- check the bounds: $O(1)$
- return cell `i`: $O(1)$

$\Rightarrow O(1)$

::::
::::column

**LinkedList**

\vspace{0.5cm}

does `i == 0 `?

- if yes, get the head:
- otherwise, get the `i-1`\textsuperscript{th} element of the queue

$\Rightarrow O(n)$

::::
:::

### Prepending

:::columns
::::column

**ArrayList**

\vspace{0.5cm}

![](graphs/newCellArrayList.png)

- create a new array large enough: $O(n)$
- write the new element: $O(1)$
- copy all the other elements after: $O(n)$

$\Rightarrow O(n)$

::::
::::column

**LinkedList**

\vspace{0.5cm}

![](graphs/newCellLinkedList.png)

- create a new cell with the new element pointing to the existing list: $O(1)$

$\Rightarrow O(1)$

::::
:::

### Performance comparison

**So which one is best?**

- if frequent random access is needed: `ArrayList`
- if frequent modification is needed: `LinkedList`

$\Rightarrow$ No "one-size-fits-all", implementation should match the use

### In any case

Previously on IST-4-JAV (class 2)…

notion of **type variable**

. . .

```Java
List<String> al = new ArrayList<String>();
```

. . .

```Java
List<String> ll = new LinkedList<String>();

```

## Associating values to keys

### A common need

- "white pages", phone books…
- Domain Name System
- looking up users in a table by their ID
- finding the selected action in a menu

```Java
interface Map<K, V> {
	…
	V get(Object k);
}
```

### Association list

:::columns
::::column

```Java
class Pair<K, V> {
	public K getKey() { … }
	public V getValue() { … }
}
```

::::
::::column

```Java
class ArrayList<T> {
	…
}
```

::::
:::

\begin{center}
$\downarrow$
\end{center}

```Java
class PhoneBook<K, V> implements Map<K,V> {
	private ArrayList<Pair<K, V>> records;
	PhoneBook (int initialSize) {
		this.records = new ArrayList<>(initialSize);
	}
}
```

### Retrieving a number from a key

```Java
…
V get(Object k) {
	for(Pair<K, V> record: this.records) {
		if(record.getKey().equals(k)) {
			return record.getValue();
		}
	}
	return null;
}
```

- must walk the phonebook until found
- on average, `this.records.size() / 2`
- $\Rightarrow O(n)$

### HashMaps

A bit of `ArrayList` and `LinkedList`!

![Structure of a HashMap](graphs/hashMap.png){width=260px}

### Properties

#### A clever implementation

- uses `.hashCode()` on the key: $O(1)$
- each list as long as the number of collisions (if `.hashCode` is good, then
  few): $O(c)$
- (see birthday problem)

#### Consequences

- fast access
- fast insertion
- resizing costs when it gets too full (initial capacity / load factor)

# Asynchronous programs

## Principles

### A key distinction

:::columns
::::{.column width=49%}

#### Regular values

- data: raw types, objects…
- can be created dynamically
- can be stored
- passed to functions

::::

::::{.column width=1%}

\rule{.1mm}{.4\textheight}

::::

::::{.column width=49%}

#### Functions

- structural unit
- (similar to loops)
- no dynamic handling

::::
:::

### Program flow

- **imperative**: "recipe", sequence of instructions
- **object**: "sections" in the program not executed linearly

$\rightarrow$ what about reactions to events?

- could the program be in "several locations" at once?
- how could each step anticipate everything that could happen? (and always the
  same anyway)
- check some central state once in a while?

### Use cases

- long (> 100 ms) calls: network
- user interaction (games, anyone?)
- graphical user interfaces (don't want everything to freeze)

How to represent reactions?

. . .

Reactions are functions so… could we pass functions after all?

## Functions as values

### So what if we could…

- store a function into a variable?
- associate it to a key? (menus…)
- pass it to another function?

```Java
boolean isEven(int n) {
	return n % 2 == 0;
}
someList.filter(isEven); // Error: cannot find symbol
```

### Interfaces!

- classes can have (several!) methods
- passing an object is a way to pass its methods
- only need a convention to find the (unique) method

$\rightarrow$ this is called an interface!

```Java
interface IntPredicate {
	public boolean run(int input);
}
```

### Representing a function

- special case: interface with *only one* method to implement
- the class is a simple "wrapper" around it
- conventional name to find it
- (can have other methods but only 1 abstract)

$\rightarrow$ it's called a functional interface (pragma `@FunctionalInterface`)
!

```Java
@FunctionalInterface
interface IntPredicate {
	public boolean run(int input);
}
```

### `isEven` as an `IntPredicate`

```Java
class IsEven implements IntPredicate {
    public boolean run(int input) {
        return input % 2 == 0;
    }
}

IntPredicate isEven = new IsEven();
isEven.run(2); // true
isEven.run(5); // false
```

### Ad-hoc inheritance

- `abstract` as a "debt" in methods
- $\rightarrow$ "settle the bill"
- you can build *ad-hoc* full-fledged classes from `interface`s and `abstract`
  ones!

. . .

```Java
IntPredicate isEven = new IntPredicate() {
	public boolean run(int input) {
		return input % 2 == 0;
	}
}
isEven.run(6); // true
```

### Leaving without paying

```Java
IntPredicate isEven = new IntPredicate();
```

. . .

```
|  Error:
|  IntPredicate is abstract; cannot be instantiated
|  IntPredicate isEven = new IntPredicate();
|                        ^----------------^
```

### Still longish

- dropped the empty `class` shell
- instantiate directly as we implement

*but*

- still several imbricated `{…}`
- have to mind the keywords

we just want to map a `<VARIABLE>` *to* an `<EXPRESSION>` (or `<STATEMENT>`)

. . .

**inline function**, aka a "\textlambda" from \textlambda-calculus, (Alonzo
Church, 1930s)

:::columns
::::{.column width=20%}
```
a -> b
```
::::
:::



### Syntax

assuming

- `(a, b, …)` is a tuple of $n$ variables (parentheses optional when 1 only)
- `<VALUE>` is an expression
- `<STATEMENT>` is a statement (often of the form `{ …; …; …; }`)

```Java
(a, b, …) -> <EXPRESSION>
(a, b, …) -> <STATEMENT>
```

are values for a given functional interface

**Examples**

```Java
(n, m) -> n+m
x -> {System.out.println(x); return -x;}
```

### A little bit shorter

The previous example becomes

```Java
IntPredicate isEven = n -> n % 2 == 0
```

- no mention of `run` any longer

. . .

- but still have to mind it!

:::columns
::::{.column width=45%}
```Java
isEven(2);
```
```
// Error:
|  cannot find symbol
|    symbol:   method isEven(int)
```
::::
::::{.column width=45%}
. . .
```Java
isEven.run(2); // true
```
::::
:::

### Method reference

- what about existing functions?
- can be wrapped into a \textlambda, but boring:
```Java
n -> someObject.someMethod(n)
```
- can't invoke regular functions except to apply them
- but you can *refer* to a method with `::`

### Example

```Java
class Arithmetic {
    …
    public static boolean isEven(int input) {
        return input % 2 == 0;
    }
    public static boolean isOdd(int input) { … }
    public static boolean isPrime(int input) { … }
    …
}
IntPredicate[] predicates = {Arithmetic::isEven,
                             Arithmetic::isPrime};
predicates[0].run(2); // true
Arithmetic.isEven(2); // true
```

### Generalizing a bit

```Java
@FunctionalInterface
interface Function<I, O> {
    public O run(I input);
}

class Multiple {
    int i;
    public Multiple(int i) {
        this.i = i;
    }
    public boolean divisible(int j) {
        return j % this.i == 0;
    }
}
```

### Full example

Remember?

```Java
someList.filter(isEven); // Error: cannot find symbol
```

. . .

```Java
List<Integer> smallerThan10 = new LinkedList<Integer>();
for(int i = 0; i < 10; i++) {
    smallerThan10.add(i);
}

Multiple by3 = new Multiple(3);
smallerThan10.removeIf(by3::divisible)
smallerThan10; // smallerThan10 ==> [1, 2, 4, 5, 7, 8]
```

# Graphical User Interfaces

## Libraries

### AWT

**Basic tooling**

- older
- events (key presses)
- notion of `Component`
- `Graphics` surface to draw
- definition of `Layout`

```Java
import java.awt.*;
import java.awt.event.*;
…
```

### Swing

**More advanced**

- more recent
- heavily depends on AWT anyway
- advanced widgets: dialogs, etc
- easier styling

```Java
import javax.swing.*;
import javax.swing.colorchooser.*;
import javax.swing.plaf.*; // pluggable look-and-feel
```

### General idea

**a tree of components**

- you plug components into containers (also components)
- recursively up to the root window
- components can appear only once

![Example window structure](graphs/nestedBox.png){height=80px}

### The 2D API

- within `AWT`
- useful for:
	+ geometric primitives
	+ text
	+ images

#### A different approach

- you implement `Component`
- a `Graphics` object is passed around in the `paint` method
- you draw to it

### Documentation

#### Packages

[https://docs.oracle.com/javase/7/docs/api/overview-summary.html](https://docs.oracle.com/javase/7/docs/api/overview-summary.html)

#### Tutorials to AWT's 2D API

[https://docs.oracle.com/javase/tutorial/2d/overview/index.html](https://docs.oracle.com/javase/tutorial/2d/overview/index.html)

#### Tutorials to Swing components

[https://docs.oracle.com/javase/tutorial/uiswing/components/index.html](https://docs.oracle.com/javase/tutorial/uiswing/components/index.html)

#### Swing examples

[https://docs.oracle.com/javase/tutorial/uiswing/examples/components/index.html](https://docs.oracle.com/javase/tutorial/uiswing/examples/components/index.html)

## Simple Swing example

### Minimal window

```Java
import javax.swing.*;        
public class EmptyWindow {
  private static void createAndShowGUI() {
    JFrame frame = new JFrame("Some window title");
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.pack();
    frame.setVisible(true);
  }
  public static void main(String[] args) {
    SwingUtilities.invokeLater(() -> createAndShowGUI());
  }
}
```

### Translation

:::columns
::::{.column width=45%}
```Java
import javax.swing.*;        
```
\vspace{1cm}
```Java
public class EmptyWindow {
```
…
```Java
}
```
::::
::::{.column width=45%}
access to

- JFrame
- SwingUtilities

\vspace{.5cm}

a class for our program
::::
:::

### Translation

```Java
private static void createAndShowGUI() {
  JFrame frame = new JFrame("Some window title");
  frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
  frame.pack();
  frame.setVisible(true);
}
```
a function to draw our window

- A window is a `JFrame`, set its title
- make the program end when the window closes
- find a size that works for components
- show the window (yes!)

### Translation

```Java
public static void main(String[] args) {
  SwingUtilities.invokeLater(() -> createAndShowGUI());
}
```

- still a program like any other, needs the usual `main`
- schedule a rendering (see the lambda?)
- where did the control flow go?

### With a panel

```Java
import java.awt.GridLayout;
  …
  private static void createAndShowGUI() {
    …
    JPanel jpanel = new JPanel(new GridLayout(2, 2));
    frame.add(jpanel);
    …
  }

```

### With a couple widgets

```Java
  …
  private static void createAndShowGUI() {
    …
    JLabel label = new JLabel("Hey there!");
    JTextField input = new JTextField();
    JButton submit = new JButton("click me");
    jpanel.add(label);
    jpanel.add(input);
    jpanel.add(submit);
    …
  }
```

### Add reactions to events

```Java
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
  …
  private static void createAndShowGUI() {
    …
    submit.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        System.out.println("Received " + e);
      }
    });
    …
  }
```

### Display image

```Java
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import javax.imageio.ImageIO;
class ImageViewer extends Component {
  private BufferedImage img;
  ImageViewer(String path) {
    try {
      img = ImageIO.read(new File(path));
    } catch (IOException e) {}
  }
  public void paint(Graphics g) {
    g.drawImage(this.img, 0, 0, null);
  }
}
```
