graphs = $(foreach f,$(1),$(patsubst %,graphs/%.png,$(f)))
pdfs = $(foreach f,$(wildcard $(1)/*.md),$(patsubst %.md,%.pdf,$(f)))

MODEL_PARALLELOGRAM = $(call graphs,$(foreach i,0 1 2 3,parallelogram$(i)))
INHERITANCE_GRAPHS = $(call graphs,classesTree exceptionsTree)
IMPLEMENTATION_GRAPHS = $(call graphs,arrayList implementations linkedList newCellArrayList newCellLinkedList hashMap)
BOX = $(call graphs,nestedBox)

SLIDES = $(call pdfs,Slides)
WORKSHEET = $(call pdfs,Worksheet)

WEBPAGE = IST-4-JAV.html

all: $(SLIDES) $(WORKSHEET) $(WEBPAGE)

Slides/Class1.pdf: Slides/Class1.md $(MODEL_PARALLELOGRAM) figures/helloworld.tex
	pandoc --slide-level=3 -t beamer $< -o $@
Slides/Class3.pdf: $(INHERITANCE_GRAPHS)
Slides/Class4.pdf: $(IMPLEMENTATION_GRAPHS)
Slides/Class4.pdf: $(BOX)

Slides/%.pdf: Slides/%.md
	pandoc --toc --slide-level=3 -t beamer $< -o $@

Worksheet/%.pdf: Worksheet/%.md
	pandoc $< -o $@

%.png: %.gv
	dot -Tpng $< -o $@

%.html: %.md
	pandoc -s $< -o $@

clean:
	rm -f $(MODEL_PARALLELOGRAM) $(INHERITANCE_GRAPHS) $(SLIDES)
